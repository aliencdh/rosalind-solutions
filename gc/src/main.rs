#![feature(iter_array_chunks)]

use std::str::FromStr;

fn gc_content(dna: &str) -> f64 {
    let gc_count = dna.chars().filter(|&ch| ch == 'C' || ch == 'G').count() as f64;
    gc_count * 100.0 / dna.len() as f64
}

struct Entry {
    label: String,
    dna: String,
}
impl From<[&str; 2]> for Entry {
    fn from(src: [&str; 2]) -> Self {
        let label = src[0].trim().chars().skip(1).collect::<String>();
        let dna = src[1].trim().to_string();
        Self { label, dna }
    }
}

fn highest_gc_content(strings: &[Entry]) -> (String, f64) {
    let mut max = f64::MIN;
    let mut rv = String::new();

    for entry in strings {
        let gc = gc_content(&entry.dna);
        if gc > max {
            max = gc;
            rv = entry.label.clone();
        }
    }

    (rv, max)
}

fn main() -> std::io::Result<()> {
    let input = std::fs::read_to_string("input")?
        .split('\n')
        .filter_map(|s| {
            let trimmed = s.trim();
            if trimmed.is_empty() {
                None
            } else {
                Some(trimmed)
            }
        })
        .array_chunks::<2>()
        .map(Entry::from)
        .collect::<Vec<_>>();

    let result = highest_gc_content(&input);
    println!("{}\n{:.6}", result.0, result.1);

    Ok(())
}
