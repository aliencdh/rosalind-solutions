import Text.Printf (printf)

parseChar :: (Int, Int, Int, Int) -> String -> (Int, Int, Int, Int)
parseChar counts [] = counts
parseChar (a, c, g, t) (x:xs) 
  = case x of
      'A' -> parseChar (a + 1, c, g, t) xs
      'C' -> parseChar (a, c + 1, g, t) xs
      'G' -> parseChar (a, c, g + 1, t) xs
      'T' -> parseChar (a, c, g, t + 1) xs
      _   -> parseChar (a, c, g, t) xs
  

main = do
  input <- getContents
  let (a, c, g, t) = parseChar (0, 0, 0, 0) input
  printf "%d %d %d %d\n" a c g t

