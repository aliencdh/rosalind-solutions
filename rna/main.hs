thymineToUracil :: Char -> Char
thymineToUracil 'T' = 'U'
thymineToUracil any = any

main = interact (map thymineToUracil)
