import Data.List.Extra (trim)

complement :: Char -> Char
-- thymine - adenine
complement 'T' = 'A'
complement 'A' = 'T'
-- guanine - cytosine
complement 'G' = 'C'
complement 'C' = 'G'

main = interact (reverse . map complement . trim)
