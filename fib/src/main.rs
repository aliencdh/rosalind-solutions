use std::str::FromStr;

pub fn fib(mul: usize, n: usize) -> usize {
    static mut CACHE: Vec<usize> = Vec::new();
    static mut INIT: bool = false;

    if unsafe { !INIT } {
        unsafe {
            INIT = true;
            CACHE.append(&mut vec![1, 1]);
        }
    }

    match unsafe { CACHE.get(n) } {
        Some(&val) => val,
        None => {
            let d1 = fib(mul, n - 1);
            let d2 = fib(mul, n - 2);
            unsafe {
                CACHE.resize(n, 0);
                CACHE.insert(n, d2 * mul + d1);
            }
            d2 * mul + d1
        }
    }
}

fn main() -> std::io::Result<()> {
    let input = std::fs::read_to_string("input")?
        .split(' ')
        .filter_map(|s| {
            let trimmed = s.trim();
            if trimmed.is_empty() {
                None
            } else {
                Some(usize::from_str(trimmed).unwrap())
            }
        })
        .collect::<Vec<_>>();

    let months = input[0] - 1;
    let mul = input[1];

    println!("{}", fib(mul, months));

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::fib;

    #[test]
    fn test_fib() {
        let mul = 3;
        assert_eq!(fib(mul, 4), 19);
    }
}
